#pragma once

#ifdef DEBUG

#define DO_THROW

#ifdef DO_THROW
#define THROW throw 0
#else // DO_THROW
#define THROW exit(1)
#endif // DO_THROW

#include <cstdlib>
#include <stdio.h>

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wgnu-zero-variadic-macro-arguments"

#define STRINGIFY(STR) STRINGIFY2(STR)
#define STRINGIFY2(STR) #STR

#define MAKE_FILE_LINK() __FILE__ ":" STRINGIFY(__LINE__)

#define PRINT_WITH_LINE(MSG, ...) printf(MAKE_FILE_LINK() ": " MSG "\n", ##__VA_ARGS__)

#define ERROR(...)                                                                                 \
	{                                                                                              \
		printf(__VA_ARGS__);                                                                       \
		THROW;                                                                                     \
	}

#define UNREACHABLE() ERROR("Unreachable code hit!")

#define ASSERT(CONDITION) ASSERT_MSG(CONDITION, "Assertion failed: " #CONDITION)

#define ASSERT_MSG(CONDITION, MSG, ...)                                                            \
	if (!(CONDITION))                                                                              \
	{                                                                                              \
		PRINT_WITH_LINE(MSG, ##__VA_ARGS__);                                                       \
		THROW;                                                                                     \
	}

// Warnings:
#define WARN(CONDITION)                                                                            \
	if (!(CONDITION)) { PRINT_WITH_LINE(Warning : CONDITION) }

#define WARN_DEPRECATED() PRINT_WITH_LINE("Warning : This method is deprecated")

#define WARN_MSG(CONDITION, MSG, ...)                                                              \
	if (!(CONDITION)) { PRINT_WITH_LINE(MSG, ##__VA_ARGS__); }

#define WARN_RET(CONDITION)                                                                        \
	if (!(CONDITION))                                                                              \
	{                                                                                              \
		PRINT_WITH_LINE(Warning : CONDITION);                                                      \
		return;                                                                                    \
	}

#define WARN_RET_MSG(CONDITION, MSG, ...)                                                          \
	if (!(CONDITION))                                                                              \
	{                                                                                              \
		PRINT_WITH_LINE(MSG, ##__VA_ARGS__);                                                       \
		return;                                                                                    \
	}

#define WARN_RET_VAL(CONDITION, RETVAL)                                                            \
	if (!(CONDITION))                                                                              \
	{                                                                                              \
		PRINT_WITH_LINE(Warning : CONDITION);                                                      \
		return RETVAL;                                                                             \
	}

#define WARN_RET_VAL_MSG(CONDITION, RETVAL, MSG, ...)                                              \
	if (!(CONDITION))                                                                              \
	{                                                                                              \
		PRINT_WITH_LINE(MSG, ##__VA_ARGS__);                                                       \
		return RETVAL;                                                                             \
	}

#pragma clang diagnostic pop

#else // DEBUG

#define ERROR(...)
#define UNREACHABLE()

#define ASSERT(CONDITION)                                                                          \
	if (!(CONDITION)) {}
#define ASSERT_MSG(CONDITION, ...)                                                                 \
	if (!(CONDITION)) {}
#define WARN(CONDITION, ...)                                                                       \
	if (!(CONDITION)) {}
#define WARN_DEPRECATED()
#define WARN_MSG(CONDITION, ...)                                                                   \
	if (!(CONDITION)) {}
#define WARN_RET(CONDITION, ...)                                                                   \
	if (!(CONDITION)) {}
#define WARN_RET_MSG(CONDITION, ...)                                                               \
	if (!(CONDITION)) {}
#define WARN_RET_VAL_MSG(CONDITION, ...)                                                           \
	if (!(CONDITION)) {}

#endif
