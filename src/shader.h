#pragma once
#include "glad/glad.h"
namespace Shader
{
	char * getSrc(const char path[]);

	GLuint loadStage(const char * src, GLenum shader);

	GLuint compile(const char vert[], const char frag[]);
}