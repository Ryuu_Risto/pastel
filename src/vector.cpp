#include "vector.h"
#include <stdio.h>

namespace linalg
{
	namespace vector
	{	
		void printvec(Vector V, const char label[], int len)
		{
			if (label) printf("%s vector-%i:",label, len);
			if (len > 0 && len < 5) printf("(");
			switch (len)
			{
				case 4: printf("% 3.2f,", V.v[3]);
				case 3: printf("% 3.2f,", V.v[2]);
				case 2: printf("% 3.2f,", V.v[1]);
				case 1: printf("% 3.2f)\n", V.v[0]);
					break;
				default:
					printf("[linalg] Error: Invalid vector");
					if (label) printf(" %s",label);
					printf(" of size %i!\n", len);
			}
		}

		Vector Vector::operator + (Vector const &vec)
		{
			Vector out;
			for (int i = 0; i < 4; i++) out.v[i] = v[i] + vec.v[i];
			return out;
		}
		Vector Vector::operator - (Vector const &vec)
		{
			Vector out;
			for (int i = 0; i < 4; i++) out.v[i] = v[i] - vec.v[i];
			return out;
		}
		Vector Vector::operator * (float const scalar)
		{
			Vector out;
			for (int i = 0; i < 4; i++) out.v[i] = v[i] * scalar;
			return out;
		}
		Vector Vector::operator / (float const scalar)
		{
			Vector out;
			for (int i = 0; i < 4; i++) out.v[i] = v[i] / scalar;
			return out;
		}

		float Vector::operator * (Vector const &vec)
		{
			return v[0]*vec.v[0] + v[1]*vec.v[1] + v[2]*vec.v[2] + v[3]*vec.v[3];
		}

		// With tab size 4, this is pure nut
		Vector 	Vector::operator ^ (Vector const &vec)
		{
			return Vector(1,0,0) * (v[1]*vec.v[2] - v[2]*vec.v[1])
				 + Vector(0,1,0) * (v[2]*vec.v[0] - v[0]*vec.v[2])
				 + Vector(0,0,1) * (v[0]*vec.v[1] - v[1]*vec.v[0]);
		}
		

		float Vector::magnitude()
		{
			return sqrt(v[0]*v[0] + v[1]*v[1] + v[2]*v[2] + v[3]*v[3]);
		}
		Vector Vector::unit()
		{
			if (magnitude() == 0) return Vector();
			return *this / magnitude();
		}
	}
}