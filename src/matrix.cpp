#include "matrix.h"
#include "vector.h"
#include <stdio.h>
#include <math.h>

#define PI 3.14159265358979323846

namespace linalg
{
	namespace matrix
	{		
		Matrix ortho3d(	float left, float right,
						float bottom, float top,
						float near, float far, bool flip_z)
		{
			Matrix M;
			M.m[0][0] = 2 / (right - left);
			M.m[1][1] = 2 / (top - bottom);
			M.m[2][2] = 2 / (far - near);
			M.m[3][0] = -(right + left) / (right - left);
			M.m[3][1] = -(top   + bottom) / (top - bottom);
			M.m[3][2] = -(far + near) / (far- near);
			M.m[3][3] = 1;

			M.m[2][0] = flip_z ? -M.m[2][0] : M.m[2][0];
			M.m[2][1] = flip_z ? -M.m[2][1] : M.m[2][1];
			M.m[2][2] = flip_z ? -M.m[2][2] : M.m[2][2];

			return M;
		}

		Matrix proj3d(float fov, float r, float n, float f)
		{
			float Sh = 1/(tan(fov/2*PI/180));
			float Sv = 2*atan(tan(Sh/2)*r);

			return Matrix(new float[16]{Sh,		0.0f,	0.0f,		0.0f,
										0.0f,	Sv,		0.0f,		0.0f,
										0.0f,	0.0f,	f/(f-n),	1.0f,
										0.0f,	0.0f,	-f*n/(f-n),	0.0f});
		}

		Matrix identity()
		{
			return Matrix(new float[16]{1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1});
		}

		Matrix Matrix::operator * (Matrix const &mat)
		{
			vector::Vector c[4] = {	vector::Vector(mat.m[0][0],mat.m[1][0],mat.m[2][0],mat.m[3][0]),
									vector::Vector(mat.m[0][1],mat.m[1][1],mat.m[2][1],mat.m[3][1]),
									vector::Vector(mat.m[0][2],mat.m[1][2],mat.m[2][2],mat.m[3][2]),
									vector::Vector(mat.m[0][3],mat.m[1][3],mat.m[2][3],mat.m[3][3])};
			vector::Vector r[4] = { vector::Vector(m[0]),vector::Vector(m[1]),vector::Vector(m[2]),vector::Vector(m[3])};
			Matrix M;
			for (int i = 0; i < 4; i++)
			{
				for (int j = 0; j < 4; j++)
				{
					M.m[i][j] = r[i]*c[j];
				}
			}
			return M;
		}

		void printma(Matrix M, const char label[])
		{
			if (label) printf("%s matrix:\n",label);
			printf("┌% 7.2f,% 7.2f,% 7.2f,% 7.2f┐\n",M.m[0][0],M.m[1][0],M.m[2][0],M.m[3][0]);
			printf("│% 7.2f,% 7.2f,% 7.2f,% 7.2f│\n",M.m[0][1],M.m[1][1],M.m[2][1],M.m[3][1]);
			printf("│% 7.2f,% 7.2f,% 7.2f,% 7.2f│\n",M.m[0][2],M.m[1][2],M.m[2][2],M.m[3][2]);
			printf("└% 7.2f,% 7.2f,% 7.2f,% 7.2f┘\n",M.m[0][3],M.m[1][3],M.m[2][3],M.m[3][3]);
		}

		Matrix Matrix::translate(vector::Vector T)
		{
			m[3][0] += T.v[0];
			m[3][1] += T.v[1];
			m[3][2] += T.v[2];
			m[3][3] += T.v[3];
			return m;
		}
		Matrix Matrix::translate(float x, float y, float z)
		{
			m[3][0] += x;
			m[3][1] += y;
			m[3][2] += z;
			return m;
		}
		Matrix Matrix::scale(vector::Vector T)
		{
			m[0][0] *= T.v[0];
			m[1][1] *= T.v[1];
			m[2][2] *= T.v[2];
			return m;
		}

		Matrix rotation(float p, float y, float r)
		{
			p *= ETR;
			y *= ETR;
			r *= ETR;

			Matrix Rx = identity();
			Matrix Ry = identity();
			Matrix Rz = identity();

			Rx.m[1][1] = cos(p); Rx.m[2][2] = cos(p);
			Rx.m[1][2] = sin(p); Rx.m[2][1] = -sin(p);
			Ry.m[0][0] = cos(y); Ry.m[2][2] = cos(y);
			Ry.m[0][2] = -sin(y); Ry.m[2][0] = sin(y);
			Rz.m[0][0] = cos(r); Rz.m[1][1] = cos(r);
			Rz.m[0][1] = sin(r); Rz.m[1][0] = -sin(r);

			return Rx*Ry*Rz;
		}

		Matrix rotation(vector::Vector rot)
		{
			return rotation(rot.v[0], rot.v[1], rot.v[2]);
		}

		float * Matrix::arr()
		{
			float * arr = (float *)(malloc(sizeof(float)*16));
			for (int i = 0; i < 4; i++) for (int j = 0; j < 4; j++) arr[i*4+j] = m[i][j];
			return arr;
		}

		Matrix Matrix::invert()
		{
			const float * marr = arr();
			float out[16], inv[16], det;
			// [Future me] I can't believe I did this. Holy shit 
			inv[0] = marr[5]*marr[10]*marr[15]-marr[5]*marr[11]*marr[14]-marr[9]*marr[6]*marr[15]+marr[9]*marr[7]*marr[14]+marr[13]*marr[6]*marr[11]-marr[13]*marr[7]*marr[10];
			inv[1] =-marr[1]*marr[10]*marr[15]+marr[1]*marr[11]*marr[14]+marr[9]*marr[2]*marr[15]-marr[9]*marr[3]*marr[14]-marr[13]*marr[2]*marr[11]+marr[13]*marr[3]*marr[10];
			inv[2] = marr[1]*marr[6] *marr[15]-marr[1]*marr[7] *marr[14]-marr[5]*marr[2]*marr[15]+marr[5]*marr[3]*marr[14]+marr[13]*marr[2]*marr[7] -marr[13]*marr[3]*marr[6];
			inv[3] =-marr[1]*marr[6] *marr[11]+marr[1]*marr[7] *marr[10]+marr[5]*marr[2]*marr[11]-marr[5]*marr[3]*marr[10]-marr[9] *marr[2]*marr[7] +marr[9] *marr[3]*marr[6];
			inv[4] =-marr[4]*marr[10]*marr[15]+marr[4]*marr[11]*marr[14]+marr[8]*marr[6]*marr[15]-marr[8]*marr[7]*marr[14]-marr[12]*marr[6]*marr[11]+marr[12]*marr[7]*marr[10];
			inv[5] = marr[0]*marr[10]*marr[15]-marr[0]*marr[11]*marr[14]-marr[8]*marr[2]*marr[15]+marr[8]*marr[3]*marr[14]+marr[12]*marr[2]*marr[11]-marr[12]*marr[3]*marr[10];
			inv[6] =-marr[0]*marr[6] *marr[15]+marr[0]*marr[7] *marr[14]+marr[4]*marr[2]*marr[15]-marr[4]*marr[3]*marr[14]-marr[12]*marr[2]*marr[7] +marr[12]*marr[3]*marr[6];
			inv[7] = marr[0]*marr[6] *marr[11]-marr[0]*marr[7] *marr[10]-marr[4]*marr[2]*marr[11]+marr[4]*marr[3]*marr[10]+marr[8] *marr[2]*marr[7] -marr[8] *marr[3]*marr[6];
			inv[8] = marr[4]*marr[9] *marr[15]-marr[4]*marr[11]*marr[13]-marr[8]*marr[5]*marr[15]+marr[8]*marr[7]*marr[13]+marr[12]*marr[5]*marr[11]-marr[12]*marr[7]*marr[9];
			inv[9] =-marr[0]*marr[9] *marr[15]+marr[0]*marr[11]*marr[13]+marr[8]*marr[1]*marr[15]-marr[8]*marr[3]*marr[13]-marr[12]*marr[1]*marr[11]+marr[12]*marr[3]*marr[9];
			inv[10]= marr[0]*marr[5] *marr[15]-marr[0]*marr[7] *marr[13]-marr[4]*marr[1]*marr[15]+marr[4]*marr[3]*marr[13]+marr[12]*marr[1]*marr[7] -marr[12]*marr[3]*marr[5];
			inv[11]=-marr[0]*marr[5] *marr[11]+marr[0]*marr[7] *marr[9] +marr[4]*marr[1]*marr[11]-marr[4]*marr[3]*marr[9] -marr[8] *marr[1]*marr[7] +marr[8] *marr[3]*marr[5];
			inv[12]=-marr[4]*marr[9] *marr[14]+marr[4]*marr[10]*marr[13]+marr[8]*marr[5]*marr[14]-marr[8]*marr[6]*marr[13]-marr[12]*marr[5]*marr[10]+marr[12]*marr[6]*marr[9];
			inv[13]= marr[0]*marr[9] *marr[14]-marr[0]*marr[10]*marr[13]-marr[8]*marr[1]*marr[14]+marr[8]*marr[2]*marr[13]+marr[12]*marr[1]*marr[10]-marr[12]*marr[2]*marr[9];
			inv[14]=-marr[0]*marr[5] *marr[14]+marr[0]*marr[6] *marr[13]+marr[4]*marr[1]*marr[14]-marr[4]*marr[2]*marr[13]-marr[12]*marr[1]*marr[6] +marr[12]*marr[2]*marr[5];
			inv[15]= marr[0]*marr[5] *marr[10]-marr[0]*marr[6] *marr[9] -marr[4]*marr[1]*marr[10]+marr[4]*marr[2]*marr[9] +marr[8] *marr[1]*marr[6] -marr[8] *marr[2]*marr[5];

			det = marr[0] * inv[0] + marr[1] * inv[4] + marr[2] * inv[8] + marr[3] * inv[12];

			if (det == 0) {printf("=== Pastel Engine Runtime Error ===\n===    Matrix Not Inversible    ===\n"); return *this;}

			det = 1.0 / det;

			for (int i = 0; i < 16; i++) out[i] = inv[i] * det;
			return Matrix(out);
		}
	}
}