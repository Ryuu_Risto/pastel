#pragma once
#include "vector.h"

namespace linalg
{
	// Euler To Radian conversion constant
	const float ETR = 0.01745329251994329576;
	namespace matrix
	{
		class Matrix
		{
			public:
			float m[4][4];

			Matrix()
			{
				for (int i = 0; i < 4; i++) for (int j = 0; j < 4; j++)
				{
					m[i][j] = 0;
				}
			}
			Matrix(float mat[4][4])
			{
				for (int i = 0; i < 4; i++) for (int j = 0; j < 4; j++)
				{
					m[i][j] = mat[i][j];
				}
			}
			Matrix(float arr[16])
			{
				for (int i = 0; i < 4; i++) for (int j = 0; j < 4; j++)
				{
					m[i][j] = arr[4*i+j];
				}
			}

			Matrix operator * (Matrix const &);
			Matrix translate(vector::Vector);
			Matrix translate(float, float, float);
			Matrix scale(vector::Vector);
			Matrix rotate(vector::Vector);
			float * arr();
			Matrix invert();
		};

		Matrix ortho3d(	float, float, float, float,
						float, float, bool = true);
		
		Matrix proj3d(float fov, float aspect, float n, float f);
		
		Matrix identity();

		Matrix rotation(float, float, float);
		Matrix rotation(vector::Vector);

		void printma(Matrix, const char []);
	}
}