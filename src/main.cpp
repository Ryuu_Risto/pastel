#define CGLTF_IMPLEMENTATION
#define breal break
#define void_pointer_arithmetic reinterpret_cast

#include <stdio.h>
#include <stdlib.h>
#include <SDL2/SDL.h>
#include "glad/glad.h"
#include "cgltf/cgltf.h"
#include "assert.h"
#include "shader.h"
#include "matrix.h"
#include "vector.h"
#include "engine.h"

pastel::Engine Engine;

struct vertex
{
	linalg::vector::Vector3 pos;
	linalg::vector::Vector3 normal;
	linalg::vector::Vector3 unusual;
};

struct modelData
{
	bool success;
	vertex * vertices;
	int vertexCount;
	unsigned short * indices;
	int indexCount;

};

struct MVP
{
	linalg::matrix::Matrix model;
	linalg::matrix::Matrix view;
	linalg::matrix::Matrix projection;
};

const char * mapma(GLenum msg)
{
	switch (msg)
	{
		case GL_DEBUG_SOURCE_API:
			return "API";
		case GL_DEBUG_SOURCE_WINDOW_SYSTEM:
			return "WINDOW";
		case GL_DEBUG_SOURCE_SHADER_COMPILER:
			return "SHADER";
		case GL_DEBUG_SOURCE_THIRD_PARTY:
			return "THIRD";
		case GL_DEBUG_SOURCE_APPLICATION:
			return "APPLICATION";
		case GL_DEBUG_SOURCE_OTHER:
			return "OTHER";
		case GL_DEBUG_TYPE_ERROR:
			return "ERROR";
		case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
			return "DEPRECATED";
		case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
			return "UNDEFINED";
		case GL_DEBUG_TYPE_PORTABILITY:
			return "PORTABILITY";
		case GL_DEBUG_TYPE_PERFORMANCE:
			return "PERFORMANCE";
		case GL_DEBUG_TYPE_MARKER:
			return "MARKER";
		case GL_DEBUG_TYPE_PUSH_GROUP:
			return "PUSH";
		case GL_DEBUG_TYPE_POP_GROUP:
			return "POP";
		case GL_DEBUG_TYPE_OTHER:
			return "OTHER";
		case GL_DEBUG_SEVERITY_HIGH:
			return "HIGH";
		case GL_DEBUG_SEVERITY_MEDIUM:
			return "MEDIUM";
		case GL_DEBUG_SEVERITY_LOW:
			return "LOW";
		case GL_DEBUG_SEVERITY_NOTIFICATION:
			return "NOTIFICATION";
	}
	return "DAFUQ";
}

void GLAPIENTRY
MessageCallback( GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length,
				 const GLchar* message, const void* userParam )
{
			if (severity == GL_DEBUG_SEVERITY_NOTIFICATION) return;
			fprintf(stderr, "=== GL %s %s %s #%i ===\n%s\n",
					mapma(source), mapma(type), mapma(severity), id, message);
}

void init(int w = 640, int h = 480)
{
	Engine.Window.size[0] = w;
	Engine.Window.size[1] = h;
	Engine.Camera.mouseSpeed = 0.05;
	Engine.Camera.fov = 110;

	if( SDL_Init( SDL_INIT_VIDEO ) < 0 )
	{
		printf("=== SDL init message ===\n");
		printf( "SDL could not initialize!\nSDL_Error: %s\n", SDL_GetError() );
	}
	else
	{
		Engine.Window.window = SDL_CreateWindow("", 	SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
										Engine.Window.size[0], Engine.Window.size[1],
										SDL_WINDOW_OPENGL);
		if (Engine.Window.window == NULL)
		{
			printf("=== SDL init message ===\n");
			printf( "Window could not be created!\nSDL_Error: %s\n", SDL_GetError() );
		}
		else
		{
			SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
			SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 6);
			SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
			Engine.Window.context = SDL_GL_CreateContext(Engine.Window.window);
			SDL_GL_SetSwapInterval(1);
			SDL_SetRelativeMouseMode(SDL_TRUE);

			gladLoadGL(); 
			glViewport(0,0,Engine.Window.size[0], Engine.Window.size[1]);
			glEnable(GL_DEPTH_TEST);

			// During init, enable debug output
			glEnable              ( GL_DEBUG_OUTPUT );
			glDebugMessageCallback( MessageCallback, 0 );

			Engine.initialize();
		}
	}
}

void quit()
{
	SDL_DestroyWindow(Engine.Window.window);
	SDL_Quit();
}

void handleEvent(SDL_Event * e, bool * q)
{
	while(SDL_PollEvent(e) != 0)
	{
		switch (e->type)
		{
			case SDL_QUIT:
				*q = true;
				break;
			case SDL_MOUSEMOTION:
				Engine.Camera.rot = Engine.Camera.rot + linalg::vector::Vector(e->motion.yrel*Engine.Camera.mouseSpeed, e->motion.xrel*Engine.Camera.mouseSpeed);
				break;
			case SDL_KEYDOWN:
				if (e->key.repeat == 1) break;
				switch(e->key.keysym.sym)
				{
					case SDLK_ESCAPE:
						*q = true;
						break;
					case SDLK_w:
						Engine.Input |= pastel::KEY_FORWARD;
						break;
					case SDLK_s:
						Engine.Input |= pastel::KEY_BACKWARD;
						break;
					case SDLK_a:
						Engine.Input |= pastel::KEY_LEFT;
						break;
					case SDLK_d:
						Engine.Input |= pastel::KEY_RIGHT;
						break;
					case SDLK_SPACE:
						Engine.Input |= pastel::KEY_JUMP;
						break;
					case SDLK_LCTRL:
						Engine.Input |= pastel::KEY_DUCK;
						break;
				}
				break;
			case SDL_KEYUP:
				switch(e->key.keysym.sym)
				{
					case SDLK_w:
						Engine.Input = Engine.Input & ~pastel::KEY_FORWARD;
						break;
					case SDLK_s:
						Engine.Input = Engine.Input & ~pastel::KEY_BACKWARD;
						break;
					case SDLK_a:
						Engine.Input = Engine.Input & ~pastel::KEY_LEFT;
						break;
					case SDLK_d:
						Engine.Input = Engine.Input & ~pastel::KEY_RIGHT;
						break;
					case SDLK_SPACE:
						Engine.Input = Engine.Input & ~pastel::KEY_JUMP;
						break;
					case SDLK_LCTRL:
						Engine.Input = Engine.Input & ~pastel::KEY_DUCK;
						break;
				}
				break;
		}
	}
}

void run()
{
	// Movement
	float speed = 0.2;
	linalg::vector::Vector m;
	m.v[1] += (Engine.Input & pastel::KEY_JUMP) != 0 ? 1 : 0;
	m.v[1] -= (Engine.Input & pastel::KEY_DUCK) != 0 ? 1 : 0;
	m.v[0] -= (Engine.Input & pastel::KEY_LEFT) != 0 ? 1 : 0;
	m.v[0] += (Engine.Input & pastel::KEY_RIGHT) != 0 ? 1 : 0;
	m.v[2] += (Engine.Input & pastel::KEY_FORWARD) != 0 ? 1 : 0;
	m.v[2] -= (Engine.Input & pastel::KEY_BACKWARD) != 0 ? 1 : 0;
	m = m.unit() * speed;
	float angleEuler = Engine.Camera.rot.v[1]*linalg::ETR;
	Engine.Camera.pos = Engine.Camera.pos + linalg::vector::Vector(
						m.v[2] * sin(angleEuler) + m.v[0] * cos(angleEuler), m.v[1],
						m.v[2] * cos(angleEuler) - m.v[0] * sin(angleEuler));
}

void render(MVP * mvp, GLuint indices)
{
	glClearColor(0.2, 0.2, 0.2, 1.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glDrawElements(GL_TRIANGLES, indices, GL_UNSIGNED_SHORT, nullptr);

	SDL_GL_SwapWindow(Engine.Window.window);
}

modelData loadModel(const char model [])
{
	modelData m = {};
	cgltf_options options = {};
	cgltf_data * data = NULL;

	cgltf_result result = cgltf_parse_file(&options, model, &data);
	if (result != cgltf_result_success) {
		printf("=== CglTF loadModel message ===\n");
		printf("Model could not be parsed!\n");
		return m;
	}
	result = cgltf_load_buffers(&options, data, model);

	if (result != cgltf_result_success) {
		printf("=== CglTF loadModel message ===\n");
		printf("Buffers could not be loaded!\n");
		return m;
	}
	result = cgltf_validate(data);

	if (result != cgltf_result_success) {
		printf("=== CglTF loadModel message ===\n");
		printf("Data did not pass validation!\n");
		return m;
	}
	printf("=== CglTF loadModel message ===\n");
	printf("Model loaded, we have %lu meshes\n",data->meshes_count);

	m.vertexCount = data->meshes->primitives->attributes->data->count;
	m.vertices = static_cast<vertex*>(malloc(sizeof(vertex)*m.vertexCount));

	for (int i = 0; i < data->meshes->primitives->attributes_count; i++)
	{
		cgltf_accessor * dataAccessor = data->meshes->primitives->attributes[i].data;
		cgltf_buffer_view * bufferView = dataAccessor->buffer_view;
		cgltf_buffer * buffer = bufferView->buffer;

		ASSERT(dataAccessor->count == m.vertexCount);
		size_t attribDataBegin = void_pointer_arithmetic<size_t>(buffer->data) + bufferView->offset;

		cgltf_attribute attrib = data->meshes->primitives->attributes[i];
		switch (attrib.type)
		{
			case cgltf_attribute_type_position:
				ASSERT(attrib.data->count == m.vertexCount);
				for (int j = 0; j < m.vertexCount; j++)
				{
					linalg::vector::Vector3 * positionPosition = void_pointer_arithmetic<linalg::vector::Vector3*>(attribDataBegin + j * dataAccessor->stride);
					linalg::vector::Vector3 position = *positionPosition;
					m.vertices[j].pos = position;
				}
				break;
			case cgltf_attribute_type_normal:
				ASSERT(attrib.data->count == m.vertexCount);
				for (int j = 0; j < m.vertexCount; j++)
				{
					linalg::vector::Vector3 * normalPosition = void_pointer_arithmetic<linalg::vector::Vector3*>(attribDataBegin + j * dataAccessor->stride);
					linalg::vector::Vector3 normal = *normalPosition;
					m.vertices[j].normal = normal;
				}
				breal;
			default:
				breal;
		}
	}

	cgltf_accessor * indexAccessor = data->meshes->primitives->indices;
	m.indices = static_cast<unsigned short*>(malloc(sizeof(unsigned int)*indexAccessor->count));
	float * indices_bulk = new float[indexAccessor->count];

	m.indexCount = cgltf_accessor_unpack_floats(indexAccessor, indices_bulk, indexAccessor->count);
	for (int i = 0; i < m.indexCount; i++)
	{
		m.indices[i] = static_cast<unsigned int>(indices_bulk[i]);
	}
	delete[] indices_bulk;
	printf("%i vertices, %i indices.\n", m.vertexCount, m.indexCount);
	
	m.success = true;
	return m;
}

int main(int argc, char* args[])
{
	bool terminate;
	SDL_Event e;
	init(1920, 1080);
	if (!Engine.initialized())
	{
		printf("=== Pastel engine critical error ===\n");
		printf("Engine failed to initialize.\nCommitting Sudoku...\n");
		throw 1;
		exit(1);
	}
	modelData bananas = {};
	bananas = loadModel("bananas.glb");

	GLuint shader = Shader::compile("src/vertex.glsl", "src/fragment.glsl");

	GLuint VAO;
	glGenVertexArrays(1, &VAO);
	glBindVertexArray(VAO);

	GLuint VBO;
	glGenBuffers(1, &VBO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertex)*bananas.vertexCount, bananas.vertices, GL_DYNAMIC_DRAW);

	GLuint loc_aPosition = glGetAttribLocation(shader, "aPosition");
	glEnableVertexAttribArray(loc_aPosition);
	glVertexAttribPointer(loc_aPosition, 3, GL_FLOAT, false, sizeof(vertex), void_pointer_arithmetic<void *>(offsetof(vertex, pos)));

	GLuint loc_aNormal = glGetAttribLocation(shader, "aNormal");
	glEnableVertexAttribArray(loc_aNormal);
	glVertexAttribPointer(loc_aNormal, 3, GL_FLOAT, false, sizeof(vertex), void_pointer_arithmetic<void *>(offsetof(vertex, normal)));
/*
	GLuint loc_aUnusual = glGetAttribLocation(shader, "aUnusual");
	glEnableVertexAttribArray(loc_aUnusual);
	glVertexAttribPointer(loc_aUnusual, 3, GL_FLOAT, false, sizeof(vertex), void_pointer_arithmetic<void *>(offsetof(vertex, unusual)));
*/
	GLuint IBO;
	glGenBuffers(1, &IBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLushort)*bananas.indexCount, bananas.indices, GL_DYNAMIC_DRAW);

	glUseProgram(shader);

	GLuint loc_uProjection = glGetUniformLocation(shader, "uProjection");
	GLuint loc_uView = glGetUniformLocation(shader, "uView");
	GLuint loc_uModel = glGetUniformLocation(shader, "uModel");

	MVP mvp;
	mvp.projection = linalg::matrix::proj3d(Engine.Camera.fov, (float)Engine.Window.size[0]/(float)Engine.Window.size[1], 1, 100);
	/*
	linalg::matrix::printma(mvp.model,"Model");
	linalg::matrix::printma(mvp.view,"View");
	linalg::matrix::printma(mvp.projection,"Proj");
	*/
	glUniformMatrix4fv(loc_uProjection, 1, GL_FALSE, mvp.projection.m[0]);

	while (!terminate)
	{
		handleEvent(&e, &terminate);
		run();


		//mvp.model = linalg::matrix::identity() * linalg::matrix::rotation(0,0,0).translate(linalg::vector::Vector(0,0,10));
		mvp.model = linalg::matrix::identity() * linalg::matrix::rotation(0,(float)SDL_GetTicks()/16,0).translate(linalg::vector::Vector(0,0,10));
		glUniformMatrix4fv(loc_uModel, 1, GL_FALSE, mvp.model.m[0]);

		mvp.view = (linalg::matrix::rotation(Engine.Camera.rot).translate(Engine.Camera.pos)).invert();
		glUniformMatrix4fv(loc_uView, 1, GL_FALSE, mvp.view.m[0]);

		render(&mvp, bananas.indexCount);
	}
	quit();
	return 0;
}