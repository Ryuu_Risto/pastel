#include "quaternion.h"
#include <math.h>

#define PI 3.14159265358979323846

namespace linalg
{
	namespace quat
	{
		Quaternion fromEuler(float pitch, float yaw, float roll)
		{
			pitch *= PI/180;
			yaw *= PI/180;
			roll *= PI/180;
			float cp = cos(pitch * 0.5);
			float sp = sin(pitch * 0.5);
			float cy = cos(yaw * 0.5);
			float sy = sin(yaw * 0.5);
			float cr = cos(roll * 0.5);
			float sr = sin(roll * 0.5);

			return 	Quaternion(
					cr * cp * cy + sr * sp * sy,
					sr * cp * cy - cr * sp * sy,
					cr * sp * cy + sr * cp * sy,
					cr * cp * sy - sr * sp * cy);
		}
		Quaternion fromEuler(vector::Vector angles)
		{
			return fromEuler(angles.v[0], angles.v[1], angles.v[2]);
		}
	}
}