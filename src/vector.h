#pragma once

#include <math.h>

namespace linalg
{
	namespace vector
	{
		class Vector
		{
			public:
			float v[4];

			Vector()
			{
				for (int i = 0; i < 4; i++)
				{
					v[i] = 0;
				}
			}
			Vector(float x, float y, float z = 0, float w = 0)
			{
				v[0] = x; v[1] = y;
				v[2] = z; v[3] = w;
			}
			Vector(float u[4])
			{
				for (int i = 0; i < 4; i++) v[i] = u[i];
			}

			Vector operator + (Vector const &);
			Vector operator - (Vector const &);
			Vector operator * (float const);
			Vector operator / (float const);

			float operator * (Vector const &);
			Vector operator ^ (Vector const &);

			float magnitude();
			Vector unit();
		};

		class Vector3
		{
			public:
			float v[3];

			Vector3()
			{
				for (int i = 0; i < 3; i++)
				{
					v[i] = 0;
				}
			}

			Vector3(Vector V)
			{
				for (int i = 0; i < 3; i++)
				{
					v[i] = V.v[i];
				}
			}
		};
		
		void printvec(Vector, const char [], int = 4);
	}
}