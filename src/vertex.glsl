#version 460 core

layout (location = 0) in vec3 aPosition;
layout (location = 1) in vec3 aNormal;
layout (location = 2) in vec3 aUnusual;
//breal

uniform mat4 uProjection;
uniform mat4 uView;
uniform mat4 uModel;

layout (location = 0) out vec4 vColor;

float rand(vec2 co){
	return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}

void main()
{
	vColor = vec4(1);
	mat4 MVP = uProjection * uView * uModel;
	gl_Position = MVP * vec4(aPosition, 1.0);
	mat3 uModelRot = mat3(
		normalize(uModel[0].xyz),
		normalize(uModel[1].xyz),
		normalize(uModel[2].xyz)
	);
	uModelRot[0][2] = -uModelRot[0][2];
	uModelRot[1][2] = -uModelRot[1][2];
	uModelRot[2][2] = -uModelRot[2][2];
	vec3 aNormalC = uModelRot * aNormal;
	vColor = vec4(0.5) + vec4(aNormalC * 0.5, 1.0);
}