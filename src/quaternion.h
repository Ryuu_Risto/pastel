#pragma once

#include "vector.h"

namespace linalg
{
	namespace quat
	{
		class Quaternion : public vector::Vector
		{
			using vector::Vector::Vector;
		};

		Quaternion fromEuler(float, float, float);
		Quaternion fromEuler(vector::Vector);
	}
}