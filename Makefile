CXX = g++
CXXFLAGS = -pedantic -O0 -g -std=c++2a
LIBS = -lSDL2 -lGL -ldl
INCLUDEDIRS = -Iinclude

build: clean compile

clean:
	rm -r ./bin/*

compile:
	$(CXX) src/*.c src/*.cpp -o ./bin/game $(LIBS) $(INCLUDEDIRS) $(CXXFLAGS)
