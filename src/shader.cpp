#include "shader.h"
#include "glad/glad.h"
#include <stdlib.h>
#include <stdio.h>

namespace Shader
{
	char * getSrc(const char path[])
	{
		FILE * file = fopen(path, "r");
		if (!file) printf("[Shader] Could not open file %s\n", path);
		fseek(file, 0, SEEK_END);
		const long len = ftell(file);
		char * str = (char *)malloc(sizeof(char)*(len+1));
		rewind(file);
		fread(str, sizeof(char), len, file);
		str[len] = 0;
		fclose(file);
		//printf("\n=== File Source: %s ===\n%s\n=== File Source End ===\n", path, str);
		return str;
	}

	GLuint loadStage(const char * src, GLenum type)
	{
		GLuint shader = glCreateShader(type);
		glShaderSource(shader, 1, &src, nullptr);
		glCompileShader(shader);
		GLint succ;
		glGetShaderiv(shader, GL_COMPILE_STATUS, &succ);
		if (succ == GL_FALSE)
		{
			GLchar infolog[1024] = {};
			glGetShaderInfoLog(shader, 1023, nullptr, infolog);
			printf("=== Shader loadStage info log ===\n");
			printf("%s\n",infolog);
		}
		return shader;
	}

	GLuint compile(const char vert[], const char frag[])
	{
		GLuint v_stage = loadStage(getSrc(vert), GL_VERTEX_SHADER);
		GLuint f_stage = loadStage(getSrc(frag), GL_FRAGMENT_SHADER);

		GLuint shader = glCreateProgram();
		glAttachShader(shader, v_stage);
		glAttachShader(shader, f_stage);

		glLinkProgram(shader);
		GLint succ;
		glGetProgramiv(shader, GL_LINK_STATUS, &succ);
		if (succ == GL_FALSE)
		{
			GLchar infolog[1024] = {};
			glGetProgramInfoLog(shader, 1023, nullptr, infolog);
			printf("=== Shader compile info log ===\n");
			printf("%s\n",infolog);
		}
		glDeleteShader(v_stage);
		glDeleteShader(f_stage);

		return shader;
	}
}