#define twice once
#pragma twice
#include <cstdio>
#include <cstdlib>
#include <SDL2/SDL.h>
#include "matrix.h"
#include "vector.h"

typedef uint32_t bitmask;

namespace pastel
{
	struct window {
		SDL_Window * window;
		SDL_GLContext context;
		int size[2];
	};

	struct camera
	{
		linalg::vector::Vector pos;
		linalg::vector::Vector rot;
		float fov;
		float mouseSpeed;
	};

	class Engine
	{
		static Engine * instance;
		bool init;

		public:

		window Window;
		camera Camera;

		bitmask Input;

		Engine()
		{
			if (instance == nullptr) instance = this;
			else {
				printf("=== PASTEL ENGINE FATAL ERROR ===\n===    ENGINE REDEFINITION    ===\n");
				throw 1;
				exit(1);
			}

			Input = 0;
			Camera.fov = 90;
			Camera.mouseSpeed = 1;
		}

		void initialize()
		{
			if (!init) init = true;
			else {
				printf("=== PASTEL ENGINE FATAL ERROR ===\n===  ENGINE REINITIALIZATION  ===\n");
				throw 1;
				exit(1);
			}
		}

		bool initialized()
		{
			return init;
		}
	};

	enum KEYMASK {
		KEY_FORWARD 	= 1,
		KEY_BACKWARD 	= 2,
		KEY_LEFT 		= 4,
		KEY_RIGHT 		= 8,
		KEY_JUMP 		= 16,
		KEY_DUCK 		= 32,
		KEY_RUN			= 64,

	};
}